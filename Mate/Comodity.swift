//
//  Comodities.swift
//  Mate
//
//  Created by sano on 2016/08/21.
//  Copyright © 2016年 sano. All rights reserved.
//

import UIKit

class Comodity{
    var id: Int
    var title: String
    var price: String
    var message: String
    var image: String
    var imageAsUIImage: UIImage{
        get{
            return Common.stringToImage(self.image)!
        }
    }
    
    init(id:Int, title:String, price:String, message:String, image:String){
        self.id = id
        self.title = title
        self.price = price
        self.message = message
        self.image = image
    }
}
