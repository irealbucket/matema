//
//  ArrayExtention.swift
//  Mate
//
//  Created by sano on 2016/08/31.
//  Copyright © 2016年 sano. All rights reserved.
//

import Foundation

extension Array {
    func find(includeElement: Element -> Bool) -> Element? {
        for (_, element) in enumerate() {
            if includeElement(element) {
                return element
            }
        }
        return nil
    }
}