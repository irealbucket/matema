//
//  Negotiation.swift
//  Mate
//
//  Created by sano on 2016/08/31.
//  Copyright © 2016年 sano. All rights reserved.
//

import Foundation

class Negotiation{
    var id:Int
    var comodityId:Int
    var userId:Int
    var comment:String
    
    init(id:Int, comodityId:Int, userId:Int, comment:String){
        self.id = id
        self.comodityId = comodityId
        self.userId = userId
        self.comment = comment
    }
}