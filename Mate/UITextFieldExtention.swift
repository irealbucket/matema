//
//  UITextFieldExtention.swift
//  Mate
//
//  Created by sano on 2016/08/31.
//  Copyright © 2016年 sano. All rights reserved.
//

import UIKit

extension UITextField {
    func addUnderline(width: CGFloat, color: UIColor) {
        let border = CALayer()
        border.frame = CGRect(x: 0, y: self.frame.height - width, width: self.frame.width, height: width)
        border.backgroundColor = color.CGColor
        self.layer.addSublayer(border)
    }
}

