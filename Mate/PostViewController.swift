//
//  PostViewController.swift
//  MateMa
//
//  Created by sano on 2016/08/05.
//  Copyright © 2016年 sano. All rights reserved.
//

import UIKit
import Alamofire
import RSKImageCropper
import FlatUIKit

class PostViewController: UIViewController{
    
    var pickedImage: UIImage!
    var pickedImageView: UIImageView!
    var titleTextField: UITextField!
    var priceTextField: UITextField!
    var messageTextField: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let buttonWidth:CGFloat = self.view.bounds.width / 2
        let buttonHeight:CGFloat = 50.0
        
        // 投稿ボタン
        let postButton = FUIButton()
        postButton.buttonColor = UIColor.turquoiseColor()
        postButton.cornerRadius = 6.0
        postButton.setTitle("投稿", forState: .Normal)
        postButton.frame = CGRectMake(0,0,buttonWidth, buttonHeight)
        postButton.addTarget(self, action: #selector(PostViewController.tappedPostButton(_:)), forControlEvents:.TouchUpInside)
        
        // キャンセルボタン
        let cancelButton = FUIButton()
        cancelButton.setTitle("キャンセル", forState: .Normal)
        cancelButton.buttonColor = UIColor.lightGrayColor()
        cancelButton.cornerRadius = 6.0
        cancelButton.frame = CGRectMake(0,0,buttonWidth, buttonHeight)
        cancelButton.addTarget(self, action: #selector(PostViewController.tappedCancelButton(_:)), forControlEvents:.TouchUpInside)
        
        // 画像を画面の50%のサイズで表示、保存する
        let width = self.view.bounds.width * 0.5
        let height = width
        
        // カメラ画像ImageViewを作成
        let pickedImageView = UIImageView(image: Common.resize(pickedImage, width: width, height: height))
        pickedImageView.frame = CGRectMake(0,0,width,height)
     
        // タイトルTextFieldを作成
        let priceTextField = FUITextField(frame: CGRectMake(0,0,self.view.bounds.width * 0.7,50))
        priceTextField.borderStyle = UITextBorderStyle.None
        priceTextField.placeholder = "値段を書いてください"
        priceTextField.addUnderline(1.0, color: UIColor.grayColor())

        // 説明TextFieldを作成
        let messageTextField = FUITextField(frame: CGRectMake(0,0,self.view.bounds.width * 0.7,50))
        messageTextField.borderStyle = UITextBorderStyle.None
        messageTextField.placeholder = "簡単な説明を書いてください"
        messageTextField.addUnderline(1.0, color: UIColor.grayColor())
        
        // 配置
        pickedImageView.center = CGPointMake(self.view.bounds.width / 2,(height/2) + 50)
        priceTextField.layer.position = CGPoint(x:self.view.bounds.width / 2,y:height + 130)
        messageTextField.layer.position = CGPoint(x:self.view.bounds.width / 2,y:height + 200)
        postButton.layer.position = CGPoint(x:self.view.bounds.width / 2,y:height + 300)
        cancelButton.layer.position = CGPoint(x:self.view.bounds.width / 2,y:height + 370)
        
        // コントロールをViewに追加する
        self.view.addSubview(pickedImageView)
        self.view.addSubview(priceTextField)
        self.view.addSubview(messageTextField)
        self.view.addSubview(postButton)
        self.view.addSubview(cancelButton)
        
        // コントロールをメンバ変数に代入
        self.pickedImageView = pickedImageView
        self.priceTextField = priceTextField
        self.messageTextField = messageTextField
    }
    
    func tappedPostButton(sender: AnyObject) {

        Alamofire.upload(
            .POST,
            "http://52.196.7.77:3000/comodities",
            multipartFormData: { multipartFormData in
                multipartFormData.appendBodyPart(data: "".dataUsingEncoding(NSUTF8StringEncoding)!, name: "title")
                multipartFormData.appendBodyPart(data: self.priceTextField.text!.dataUsingEncoding(NSUTF8StringEncoding)!, name: "price")
                multipartFormData.appendBodyPart(data: self.messageTextField.text!.dataUsingEncoding(NSUTF8StringEncoding)!, name: "message")
                multipartFormData.appendBodyPart(data: Common.imageToString(self.pickedImageView.image).dataUsingEncoding(NSUTF8StringEncoding)!, name: "image")
            },
            encodingCompletion: { encodingResult in
                switch encodingResult {
                    case .Success(let upload, _, _):
                        upload.responseJSON { response in
                            debugPrint(response)
                            self.dismissViewControllerAnimated(true, completion: nil)
                        }
                    case .Failure(let encodingError):
                        print(encodingError)
                }
            }
        )

    }
    func tappedCancelButton(sender: AnyObject) {
        self.dismissViewControllerAnimated(true, completion: nil)
    }
    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
        self.view.endEditing(true)
    }
}