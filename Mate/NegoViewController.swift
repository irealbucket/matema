//
//  NegoViewController.swift
//  Mate
//
//  Created by sano on 2016/08/31.
//  Copyright © 2016年 sano. All rights reserved.
//
import UIKit
import FlatUIKit

class NegoViewController:UIViewController{
    
    var selectedComodity:Comodity!
    @IBOutlet weak var commentTextField: UITextField!
    @IBOutlet weak var sendButton: FUIButton!
    
    override func viewDidLoad() {
        
        // 送信ボタン
        let sendButton = FUIButton()
        sendButton.buttonColor = UIColor.turquoiseColor()
        sendButton.cornerRadius = 6.0
        sendButton.setTitle("送信", forState: .Normal)
        self.sendButton = sendButton
        
        
        // 画像を画面の50%のサイズで
        let imageView = UIImageView(image: selectedComodity.imageAsUIImage)
        imageView.frame = CGRectMake(0,0,self.view.bounds.width * 0.5, self.view.bounds.width * 0.5)
        
        let titleLabel = UILabel()
        titleLabel.frame = CGRectMake(0,0,self.view.bounds.width * 0.7, 50)
        
        
    }
    
}
