//
//  Common.swift
//  Mate
//
//  Created by sano on 2016/08/20.
//  Copyright © 2016年 sano. All rights reserved.
//

import Foundation
import UIKit

class Common {
    class func imageToString(image :UIImage!) -> String!{
        
        //画像をNSDataに変換
        let data:NSData! = UIImagePNGRepresentation(image)
        
        if data != nil{
            
            //BASE64のStringに変換する
            let encodeString:String =
                data.base64EncodedStringWithOptions(NSDataBase64EncodingOptions.Encoding64CharacterLineLength)
            
            return encodeString
        }
        return nil
    }
    
    class func stringToImage(imageString:String) -> UIImage?{
        
        //空白を+に変換する
        let base64String = imageString.stringByReplacingOccurrencesOfString(" ", withString:"+")
        
        //BASE64の文字列をデコードしてNSDataを生成
        let decodeBase64:NSData? =
            NSData(base64EncodedString:base64String, options: NSDataBase64DecodingOptions.IgnoreUnknownCharacters)
        
        //NSDataの生成が成功していたら
        if let decodeSuccess = decodeBase64 {
            
            //NSDataからUIImageを生成
            let img = UIImage(data: decodeSuccess)
            
            //結果を返却
            return img
        }
        return nil
    }
    
    class func resize(image: UIImage, width: CGFloat, height: CGFloat) -> UIImage {
        
        let size: CGSize = CGSize(width: width, height: height)
        UIGraphicsBeginImageContext(size)
        image.drawInRect(CGRectMake(0, 0, size.width, size.height))
        
        let resizeImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return resizeImage
    }
}