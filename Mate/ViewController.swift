//
//  ViewController.swift
//  MateMa
//
//  Created by sano on 2016/08/04.
//  Copyright © 2016年 sano. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import RSKImageCropper
import FlatUIKit

class ViewController: UIViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate, RSKImageCropViewControllerDelegate,RSKImageCropViewControllerDataSource{
    
    @IBOutlet var newArrivals:UIScrollView!
    @IBOutlet var imageCamera:UIImageView!
    var trimmingWidth:CGFloat = 0.0
    var trimmingHeight:CGFloat = 0.0
    var comodities:[Comodity] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.view.backgroundColor = UIColor(red:0.9, green:1.0, blue:0.9, alpha:1.0)
        
        // カメラ画像ににTapGestureを紐付ける
        let tapGesture:UITapGestureRecognizer = UITapGestureRecognizer(target:self, action:#selector(didTapImageCamera))
        imageCamera.addGestureRecognizer(tapGesture)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        
        // 記事を取得
        comodities = []
        Alamofire.request(.GET, "http://52.196.7.77:3000/comodities")
            .responseJSON{ response in
                if response.result.value == nil {
                    return
                }
                JSON(response.result.value!).forEach{(_, json) in
                    
                    // 記事を生成
                    let comodity = Comodity(
                        id:      json["id"].intValue,
                        title:   json["title"].string!,
                        price:   json["price"].string!,
                        message: json["message"].string!,
                        image:   json["image"].string!)
                    
                    // 配列に追加
                    self.comodities.append(comodity)
                }
                
                // 画面コントロールのサイズを設定
                let margin:CGFloat = 10.0
                let frameWidth:CGFloat = self.view.bounds.width / 2 - 10
                let imageWidth:CGFloat = frameWidth - 20.0
                let imageHeight:CGFloat = imageWidth
                let priceWidth:CGFloat = frameWidth - 20.0
                let priceHeight:CGFloat = 25.0
                let messageWidth:CGFloat = frameWidth - 20.0
                let messageHeight:CGFloat = 50.0
                let getButtonWidth:CGFloat = frameWidth - 20
                let getButtonHeight:CGFloat = 30.0
                let frameHeight:CGFloat = 10 + imageHeight + priceHeight + messageHeight + getButtonHeight + 10
                var nextHeight:CGFloat = 10.0
                var columnCenterPosition:CGFloat = 0.0
                var leftPosition = true
                
                // 記事を画面に追加
                for comodity in self.comodities.reverse(){
                    
                    // フレーム
                    let frame = UIView()
                    frame.frame = CGRectMake(0,0,frameWidth, frameHeight)
                    frame.backgroundColor = UIColor.whiteColor()
                    frame.layer.cornerRadius = 10
                    
                    // 画像
                    let imageView = UIImageView(image: comodity.imageAsUIImage)
                    imageView.frame = CGRectMake(0,0,imageWidth, imageHeight)
                    
                    // 値段ラベル
                    let priceLabel = UILabel()
                    priceLabel.text = comodity.price
                    priceLabel.numberOfLines = 0
                    priceLabel.frame = CGRectMake(0,0,priceWidth, priceHeight)
                    
                    // 説明文ラベル
                    let messageLabel = UILabel()
                    messageLabel.text = comodity.message
                    messageLabel.numberOfLines = 0
                    messageLabel.frame = CGRectMake(0,0,messageWidth, messageHeight)
                    
                    // 欲しいボタン
                    let getButton = FUIButton()
                    getButton.setTitle("欲しい！", forState: .Normal)
                    getButton.buttonColor = UIColor.turquoiseColor()
                    getButton.cornerRadius = 6.0
                    getButton.tag = comodity.id
                    getButton.frame = CGRectMake(0,0,getButtonWidth, getButtonHeight)
//                    getButton.addTarget(self, action: #selector(ViewController.tappedGetButton(_:)), forControlEvents:.TouchUpInside)
                    
                    // 配置場所左決め
                    if leftPosition == true{
                        columnCenterPosition = self.view.bounds.width * 0.25 + 2.5
                    }else{
                        columnCenterPosition = self.view.bounds.width * 0.75 - 2.5
                    }
                    
                    // 配置決め
                    frame.layer.position = CGPoint(x:columnCenterPosition,y:nextHeight + frameHeight / 2)
                    imageView.layer.position = CGPoint(x:frameWidth / 2,y:margin + imageHeight / 2)
                    priceLabel.layer.position = CGPoint(x:frameWidth / 2,y:margin + imageHeight + priceHeight / 2)
                    messageLabel.layer.position = CGPoint(x:frameWidth / 2,y:margin + imageHeight + priceHeight + messageHeight / 2)
                    getButton.layer.position = CGPoint(x:frameWidth / 2,y:margin + imageHeight + priceHeight + messageHeight + getButtonHeight / 2)
                    
                    // 追加
                    frame.addSubview(imageView)
                    frame.addSubview(priceLabel)
                    frame.addSubview(messageLabel)
                    frame.addSubview(getButton)
                    self.newArrivals.addSubview(frame)
                    
                    // 右列に出力したら次の高さへ
                    if leftPosition == false{
                        nextHeight = nextHeight+frameHeight + 10
                    }
                    
                    // 左右を入れ替える
                    leftPosition = !leftPosition
                }
                
                // スクロールサイズを設定
                self.newArrivals.contentSize = CGSize(width:self.view.bounds.width,height:self.view.bounds.height)
                self.newArrivals.contentInset=UIEdgeInsetsMake(0, 0, nextHeight  + 100, 0)
        }
    }
    
    func tappedGetButton(sender: UIButton){
        
        // 選択された記事を取得
        let comodity_id = sender.tag
        let selectedComodity:Comodity! = comodities.find({$0.id == comodity_id})!
        
        let storyboard: UIStoryboard = self.storyboard!
        let negoViewController = storyboard.instantiateViewControllerWithIdentifier("nego") as! NegoViewController
        
        // 選択された画像をNegoViewControllerに設定
        negoViewController.selectedComodity = selectedComodity
        self.presentViewController(negoViewController, animated: true, completion: nil)
        
    }
    
    //写真アイコンTap処理
    func didTapImageCamera(sender: UITapGestureRecognizer){
        
        // ピッカーを定義
        let cameraPicker = UIImagePickerController()
        cameraPicker.delegate = self
        
        // カメラを立ち上げられる場合、カメラを起動
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.Camera) {
            
            // ピッカーのタイプにカメラを指定
            cameraPicker.sourceType = UIImagePickerControllerSourceType.Camera
            
            
            // カメラが立ちあげられない場合、カメラロールを起動
        }else{
            
            // ピッカーのタイプにカメラロールを指定
            cameraPicker.sourceType = UIImagePickerControllerSourceType.PhotoLibrary
        }
        
        // カメラ画面を最前面に
        self.presentViewController(cameraPicker, animated: true, completion: nil)
    }
    
    // カメラ撮影キャンセルの処理
    func imagePickerControllerDidCancel(picker: UIImagePickerController) {
        picker.dismissViewControllerAnimated(true, completion: nil)
    }
    
    //カメラPicker完了の処理
    func imagePickerController(controller: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : AnyObject]) {

        // カメラPickerで選択された画像を取得
        let pickedImage = info[UIImagePickerControllerOriginalImage] as! UIImage
   
        // 画面サイズに合わせて受け取ったトリミングリサイズを設定
        if pickedImage.size.height > pickedImage.size.width {
            trimmingWidth = self.view.bounds.width
            trimmingHeight = self.view.bounds.width
        }else{
            trimmingHeight =  pickedImage.size.height * self.view.bounds.width / pickedImage.size.width
            trimmingWidth = trimmingHeight
        }
        
        // トリミング画面ViewController
        let imageCropVC: RSKImageCropViewController = RSKImageCropViewController(image: pickedImage, cropMode: RSKImageCropMode.Custom)
        
        imageCropVC.delegate = self
        imageCropVC.dataSource = self
        
        controller.dismissViewControllerAnimated(true, completion: nil)
        self.presentViewController(imageCropVC, animated: true, completion: nil)
    }
    
    // トリミング完了処理
    func imageCropViewController(controller: RSKImageCropViewController, didCropImage croppedImage: UIImage, usingCropRect cropRect: CGRect) {
        
        // 同一StoryBordから次画面のControllerを取得
        let storyboard: UIStoryboard = self.storyboard!
        let postViewController = storyboard.instantiateViewControllerWithIdentifier("post") as! PostViewController
    
        // トリミングされた画像をPostViewControllerに設定
        postViewController.pickedImage = croppedImage
        controller.dismissViewControllerAnimated(true, completion: nil)
        self.presentViewController(postViewController, animated: true, completion: nil)
    }
    
    // トリミングマスク処理
    func imageCropViewControllerCustomMaskRect(controller: RSKImageCropViewController) -> CGRect {
        
        let maskSize = CGSizeMake(trimmingWidth, trimmingHeight)
        let viewWidth: CGFloat = CGRectGetWidth(controller.view.frame)
        let viewHeight: CGFloat = CGRectGetHeight(controller.view.frame)
        let maskRect: CGRect = CGRectMake((viewWidth - maskSize.width) * 0.5, (viewHeight - maskSize.height) * 0.5, maskSize.width, maskSize.height)
        return maskRect
    }
    
    // トリミング領域
    func imageCropViewControllerCustomMaskPath(controller: RSKImageCropViewController) -> UIBezierPath{
        let rect: CGRect = controller.maskRect
        
        let point1: CGPoint = CGPointMake(CGRectGetMinX(rect), CGRectGetMaxY(rect))
        let point2: CGPoint = CGPointMake(CGRectGetMaxX(rect), CGRectGetMaxY(rect))
        let point3: CGPoint = CGPointMake(CGRectGetMaxX(rect), CGRectGetMinY(rect))
        let point4: CGPoint = CGPointMake(CGRectGetMinX(rect), CGRectGetMinY(rect))
        
        let square: UIBezierPath = UIBezierPath()
        square.moveToPoint(point1)
        square.addLineToPoint(point2)
        square.addLineToPoint(point3)
        square.addLineToPoint(point4)
        square.closePath()
        
        return square
    }
    
    func imageCropViewControllerCustomMovementRect(controller: RSKImageCropViewController) -> CGRect {
        return controller.maskRect
    }
    
    // トリミングキャンセル処理
    func imageCropViewControllerDidCancelCrop(controller: RSKImageCropViewController) {
        self.dismissViewControllerAnimated(true, completion: nil)
    }
}

